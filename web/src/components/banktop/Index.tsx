import React from 'react';
import "./css/style.css";
import { BackTop } from 'antd';
import { VerticalAlignTopOutlined } from '@ant-design/icons';

type Props = {}

const Index = (props: Props) => {
    // const back_top = ()=>{
    //     document.documentElement.scrollTop = document.body.scrollTop = 0
    // }
    const style: React.CSSProperties = {
        height: 40,
        width: 40,
        lineHeight: '40px',
        borderRadius: 50,
        backgroundColor: '#000',
        color: '#fff',
        textAlign: 'center',
        fontSize: 25,
        
      };
    return (
        <div>
            <div>
            <BackTop style={style}>
      <div><VerticalAlignTopOutlined/></div>
            </BackTop>
            </div>
        </div>
    )
}

export default Index