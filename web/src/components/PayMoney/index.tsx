import React, { useState } from 'react'
import "./css/style.scss"
import {ArticleListItem} from "../../utils/types"
import { Button, Modal } from 'antd';
import { useNavigate } from 'react-router-dom';

type Props = {
    details_item:Array<ArticleListItem>
}

const index = (props: Props) => {
    const {details_item} = props
    const [isModalOpen, setIsModalOpen] = useState(true);
    const navigate = useNavigate()
    const handleOk = () => {
        location.href="https://openapi.alipaydev.com/gateway.do?method=alipay.trade.page.pay&app_id=2021000121670862&charset=utf-8&version=1.0&sign_type=RSA2&timestamp=2022-09-24%2016%3A56%3A39&notify_url=https%3A%2F%2Fwww.baidu.com&return_url=http%3A%2F%2Flocalhost%3A3000%2F&sign=A7Z87f0%2FVVYJauho8t2dhb1dBH%2F69G7%2FWd%2BlVBLT%2FAgphldhi8lNQYLTWUQNTwvUBhgdTHRsOO2wMKI%2B9rGWbupEPoNIuEGgofKK%2B8Mz5X5XpIlMGMwhUl0nd6S5nckEerHzeoYZvYyo5ccYxBMnPWI84yoMZnAu%2FmDarEieTaaD2%2BhUHCvarL0O93s9nt%2F94rqpbYyTSj1igrw9hZHhiw6WYVhBPbUql0iApnRkfdVFQGe%2FiuJFJ0I3bW%2Fjw7AEqEkkFsG7u9aI0iaPfTSlc6LRsnqOFlGZ2UQf2GGKSwwdzkK79ZdezM548iVBmNdLuDZl6wcuzO6F3xr5Gbueiw%3D%3D&alipay_sdk=alipay-sdk-nodejs-3.2.0&biz_content=%7B%22out_trade_no%22%3A%2215693801273221%22%2C%22product_code%22%3A%22FAST_INSTANT_TRADE_PAY%22%2C%22total_amount%22%3A%220.01%22%2C%22subject%22%3A%22%E5%95%86%E5%93%81%22%2C%22body%22%3A%22%E5%95%86%E5%93%81%E8%AF%A6%E6%83%85%22%7D"
        setIsModalOpen(false);
    };
    
    const handleCancel = () => {
        setIsModalOpen(false);
        navigate(-1)
    };
    return (
        <div className='pay_content'>
            <Modal title="确定以下收费信息" open={isModalOpen} onOk={handleOk} onCancel={handleCancel} okText="立即支付" cancelText="取消">
                <div>支付金额: <span>{details_item[0].views}</span></div>
            </Modal>
        </div>
    )
}

export default index