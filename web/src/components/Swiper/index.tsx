import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { all, QUANJUITEM, RootDispatch } from '../../utils/types'
import { Carousel } from 'antd';
import { details_find } from '../../api/action';

type Props = {

}

function Index() {
    const dispatch: RootDispatch = useDispatch()
    const [arr, setCount] = useState([])
    const { articleList } = useSelector(({ reducer }: QUANJUITEM) => {
        return {
            ...reducer
        }
    })
    const navigate = useNavigate();
    useEffect(() => {
        setCount(articleList.filter((item: all) => {
            return item.cover
        }))
    }, [articleList])

    
    // 详情
    const detailBtn = (id: string) => {
        navigate(`/home/details/${id}`)
        dispatch(details_find(id))
    }
    return (
        <div style={{
            width: '97%',
            margin: '10px auto',
        }}>
            <Carousel autoplay>
                {
                    arr.length ? arr.map((item: all) => {
                        return <div key={item.id} onClick={() => detailBtn(item.id)}>
                            <h3 style={{
                                height: '300px',
                                color: '#fff',
                                lineHeight: '160px',
                                textAlign: 'center',
                                backgroundImage: `url(${item.cover})`,
                                backgroundSize: '100% 100%'
                            }}>{item.title} <br />
                                <span style={{ paddingLeft: "10px", display: "inline-block" }}>{Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60) < 24 ? `${'大约'}${Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60)}${'小时前'}` : `${'大约'}${Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60 / 24)}${'天前'}`}</span>
                            </h3>
                        </div>
                    }) : ""
                }


            </Carousel >

        </div >
    )
}

export default Index