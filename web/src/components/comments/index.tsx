import { useState } from "react";
import Login from "../login/index"
import { MessageOutlined  } from '@ant-design/icons';
const Comments = ({ list }:any) => {
    const [isLoginModalOpen, setIsLoginModalOpen] = useState(false);
    // 随机颜色
    const commentsColor = () => `#${Math.random().toString(16).substr(2, 6)}`
    return (
        
        <ul >
            <li >
                {
                    list && list.map((item:any) => {
                        return (
                            <ul className="comments">
                                <li>
                                    <span id="comments_img" style={{ background: commentsColor() }}>

                                    </span>
                                    <span><b>{item.name}</b></span></li>
                                <li dangerouslySetInnerHTML={{ __html: item.html }} className="comments_html"></li>
                                <li>
                                    <span className="comments_date">
                                        {item.userAgent}
                                    </span>
                                    {/* 时间 */}
                                    <span>
                                        {Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60) < 24 ?
                                            `${'大约'}${Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60)}${'小时前'}` :
                                            `${'大约'}${Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60 / 24)}${'天前'}`
                                        }
                                    </span>

                                    <span  onClick={() => setIsLoginModalOpen(true)}> <MessageOutlined /> 回复 </span>
                                    {/* {
                                        flag && ind === index ? <CommentInputBox isRelease={true} Stow={() => setflag(false)} /> : <></>
                                    } */}
                                </li>
                                {
                                    item.children && <Comments list={item.children} />
                                }
                            </ul>
                        )
                    })
                }
            </li>
        </ul>
    )
}
export default Comments