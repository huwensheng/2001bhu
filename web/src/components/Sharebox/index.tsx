import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ShareId } from '../../api/action';
import './css/index.css';
import QRCode from 'qrcode.react';
import style from "../../css/index.module.css"
import { all } from '../../utils/types';



function Index() {
    const [count, setCount] = useState('')
    const dispatch: any = useDispatch();
    let obj: any = '';
    // 调取仓库数据
    const { articleList, Share_id, imgList } = useSelector(({ reducer }: any) => {
        return {
            ...reducer,
        };
    });
    const Shareno = () => {
        const Sharebox: any = document.querySelector('.Sharebox');
        Sharebox.style.display = 'none';
        dispatch(ShareId(''));
    };
    if (Share_id != '') {
        obj = articleList.filter((item: any) => {
            return item.id == Share_id;
        });
        if (!obj.length) {
            obj = imgList.filter((item: any) => {
                return item.id == Share_id;
            });
        }
    }

    const download = async () => {
        // message.error('This is an error message');
        // 获取二维码
        var mycanvas1 = await document.getElementsByTagName('canvas')[1];

        var image = await new Image();
        // 二维码图片路径
        image.src = await mycanvas1.toDataURL("image/png");

        // 获取图片
        var mycanvas: all = await document.querySelector("#imgs");

        var canvas: all = document.getElementById("canvasImg");
        var ctx: all = canvas.getContext('2d');

        ctx.fillStyle = '#fff';
        ctx.fillRect(0, 0, 400, 400);

        // // 开始绘画
        if (mycanvas !== null) {
            mycanvas.onload = function () {//图片加载完，再draw 和 toDataURL
                ctx.beginPath();
                ctx.drawImage(mycanvas, 10, 10, 384, 200);
            };

        }
        ctx.beginPath();
        ctx.moveTo(0, 100);
        ctx.lineTo(0, 100);
        ctx.fillStyle = '#000';
        ctx.font = 'normal bold 18px sans-serif';
        ctx.fillText(obj[0].title, 20, 250);
        ctx.drawImage(image, 20, 270);
        ctx.beginPath();
        ctx.font = "13px Microsoft YaHei";
        ctx.fillText("Scan the QB code to read the article", 140, 285);
        ctx.fillStyle = "#ccc";
        ctx.fillText("Original shared fromikun", 140, 345);
        ctx.fillStyle = "red";
        ctx.font = "oblique 15px Arial"
        ctx.fillText("ikun", 342, 345);

        var mycanvas1s = await document.getElementsByTagName('canvas')[0];
        mycanvas1s.onload = await function () {//图片加载完，再draw 和 toDataURL
            mycanvas1s = document.getElementsByTagName('canvas')[0];
        };
        var Allimage = new Image();
        if (!mycanvas1s.toDataURL) {
            mycanvas1s = await document.getElementsByTagName('canvas')[0];
        }
        Allimage.src = await mycanvas1s.toDataURL("image/png")
        setCount(Allimage.src);
    };
    useEffect(() => {
        download()
    }, [])
    return (
        <div className="Sharebox" >
            <canvas id="canvasImg" width={'400px'} height={'400px'} style={{ display: 'none' }}></canvas>

            <div className="childbox" id={style.dark_min}>
                <p>
                    <span>Share Poster</span>
                    <span onClick={Shareno}>X</span>
                </p>
                <p>
                    {obj.length
                        ? obj.map((item: any) => {
                            return (
                                <dl key={item.id}>
                                    {item.cover ? (
                                        <img src={item.cover} />
                                    ) : (
                                        ''
                                    )}
                                    <b>{item.title}</b>
                                    <dt>
                                        <span>
                                            <QRCode
                                                value="123" //value参数为字符串类型
                                                size={100} //二维码的宽高尺寸
                                                fgColor="#000000" //二维码的颜色
                                            />
                                        </span>
                                        <span>
                                            <i>Scan the QB code to read the article</i>
                                            <br />
                                            <br />
                                            <br />
                                            <i className="is">
                                                Original shared from
                                                <span className="on">ikun</span>
                                            </i>
                                        </span>
                                    </dt>
                                </dl>
                            );
                        })
                        : ''}
                </p>
                <p>
                    <span onClick={download}>
                        <a href={count} download="canvas.png">下载</a>
                    </span>
                    <span onClick={Shareno}>关闭</span>
                </p>
            </div>
        </div>
    );
}

export default Index;


