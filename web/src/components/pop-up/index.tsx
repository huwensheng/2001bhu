import React ,{useState}from 'react'
import { Modal } from 'antd';
import {useDispatch,useSelector} from "react-redux"
import {DATATYPE,KNOWSHARETYPE} from "./know.d"
import QRCode from "qrcode.react";

// interface PROPS{
//     isModalOpen:boolean
// }

function Know_share() {
   
    const dispatch=useDispatch()
    const isModalOpen=useSelector((state:DATATYPE)=>{
        //@ts-ignore
        return state.redux.isModalOpen
    })

    const KnowShare_item=useSelector((state:DATATYPE)=>{
        //@ts-ignore
        return state.redux.KnowShare_item
    })

    const handleOk = () => {
        dispatch({
            type:KNOWSHARETYPE,
            payload:!isModalOpen
        })
    };
    
    const handleCancel = () => {
        dispatch({
            type:KNOWSHARETYPE,
            payload:!isModalOpen
        })
    };

  return (
    <div>
        <Modal title={KnowShare_item.title} open={isModalOpen} onOk={handleOk} onCancel={handleCancel} okText="下载" cancelText="关闭">
            <div className="share_box">
                {
                    KnowShare_item.cover?<img src={KnowShare_item.cover} alt="" />:''
                }
                <p>{KnowShare_item.title}</p>
                <p>{KnowShare_item.summary}</p>
                <p>
                <QRCode
                                            value={KnowShare_item.id} //alue参数为生成二维码的链接
                                            size={100} //二维码的宽高尺寸
                                            fgColor="#000000"  //二维码的颜色
                                        />
                </p>
                <li className='share_box_liw'>识别二维码查看文章</li>
                <li className='share_box_liy'>原文分享来自爱 <span className='share_box_liy_span'>ikun</span></li>
            </div>
        </Modal>
    </div>
  )
}

export default Know_share

