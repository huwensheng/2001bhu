import {type} from "os"

export interface DATATYPE{
    knowStoreList:KNOWLISTTYPE[],
    isModalOpen:boolean,
    KnowShare_item:string
}

export interface KNOWLISTTYPE{
    "id": "string",
    "parentId": "string",
    "order": 0,
    "title": "string",
    "cover": "string",
    "summary": "string",
    "content": "string",
    "html": "string",
    "toc": "string",
    "status": "string",
    "views": 0,
    "likes": 0,
    "isCommentable": true,
    "publishAt": "2022-09-15T06:59:30.877Z",
    "createAt": "2022-09-15T06:59:30.878Z",
    "updateAt": "2022-09-15T06:59:30.878Z"
  }

export const KNOWTYPE="setknow"
export type KNOWTYPE =typeof KNOWTYPE
export interface KNOWTYPEACTION{
    type:KNOWTYPE,
    payload:KNOWLISTTYPE[]
}

export const KNOWSHARETYPE="setknow_share"
export type KNOWSHARETYPE =typeof KNOWSHARETYPE
export interface KNOWSHARETYPEACTION{
    type:KNOWSHARETYPE,
    payload:boolean
}

export const KNOWSHAREIDTYPA="setknow_share_id"
export type KNOWSHAREIDTYPA=typeof KNOWSHAREIDTYPA
export interface KNOWSHAREIDTYPACTION{
    type:KNOWSHAREIDTYPA,
    payload:KNOWLISTTYPE
}

export type ALLACTION=KNOWTYPEACTION|KNOWSHARETYPEACTION|KNOWSHAREIDTYPACTION