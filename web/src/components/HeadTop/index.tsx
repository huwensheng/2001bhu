
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { TranslationOutlined, SearchOutlined } from '@ant-design/icons';
import "./css/index.css";
import i18n from "../../i18n"
import useClient from "../../hooks/useClient"
import NavLink from "./navLink/index"
import { Button, Drawer } from 'antd';
import style from "../../css/index.module.css"



type Props = {

}

const isLight = () => {
    const currentDate = new Date();
    return currentDate.getHours() > 6 && currentDate.getHours() < 19
}
function HeadTop(props: Props) {
    const [ lang, setLang ] = useState(() => navigator.language)
    const [ flag, setFlag ] = useState( true )
    const navigate = useNavigate()
    const back_search = () => {
        navigate("/search")
    }
    const size = useClient()
    const [theme, setTheme] = useState(() => isLight() ? "白" : "黑")
    const change_language = () => {
        const newLang = lang === "cn" ? "zh-EN" : "cn"
        setLang(newLang)
        // 调用i18n的changeLanguage方法
        i18n.changeLanguage(newLang)
    }
    useEffect(() => {
        // 更改主题 更改根节点的主题
        document.documentElement.className = theme === "白" ? "light" : "dark"
    }, [theme])

    const [open, setOpen] = useState(false);

    const showDrawer = () => {
        setOpen(!open);
        setFlag(!flag)
    };

    const onClose = () => {
        setOpen(false);
    };
    return (
        <div className='HeadTop'>
            <div className="headImg">
                <div className="imgbox">
                    <img src="https://bwcreation.oss-cn-beijing.aliyuncs.com/2021-09-17/u%3D2156833431%2C1671740038%26fm%3D26%26gp%3D0.jpg" alt="" />
                </div>
            </div>
            {
                size.width > 760 ?
                    <div className='head_right'>
                        <NavLink></NavLink>
                        <div className='rigthbox'>
                            <span className='language' onClick={() => change_language()}>
                                <TranslationOutlined />
                            </span>
                            <span className='theme'>
                                <button className='theme_btn' onClick={() => {
                                    setTheme(theme === "白" ? "黑" : "白")
                                }}>{theme === "白" ? <svg className="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2739" width="32" height="32"><path d="M501.48 493.55m-233.03 0a233.03 233.03 0 1 0 466.06 0 233.03 233.03 0 1 0-466.06 0Z" fill="#F9C626" p-id="2740"></path><path d="M501.52 185.35H478.9c-8.28 0-15-6.72-15-15V87.59c0-8.28 6.72-15 15-15h22.62c8.28 0 15 6.72 15 15v82.76c0 8.28-6.72 15-15 15zM281.37 262.76l-16 16c-5.86 5.86-15.36 5.86-21.21 0l-58.52-58.52c-5.86-5.86-5.86-15.36 0-21.21l16-16c5.86-5.86 15.36-5.86 21.21 0l58.52 58.52c5.86 5.86 5.86 15.35 0 21.21zM185.76 478.48v22.62c0 8.28-6.72 15-15 15H88c-8.28 0-15-6.72-15-15v-22.62c0-8.28 6.72-15 15-15h82.76c8.28 0 15 6.72 15 15zM270.69 698.63l16 16c5.86 5.86 5.86 15.36 0 21.21l-58.52 58.52c-5.86 5.86-15.36 5.86-21.21 0l-16-16c-5.86-5.86-5.86-15.36 0-21.21l58.52-58.52c5.85-5.86 15.35-5.86 21.21 0zM486.41 794.24h22.62c8.28 0 15 6.72 15 15V892c0 8.28-6.72 15-15 15h-22.62c-8.28 0-15-6.72-15-15v-82.76c0-8.28 6.72-15 15-15zM706.56 709.31l16-16c5.86-5.86 15.36-5.86 21.21 0l58.52 58.52c5.86 5.86 5.86 15.36 0 21.21l-16 16c-5.86 5.86-15.36 5.86-21.21 0l-58.52-58.52c-5.86-5.85-5.86-15.35 0-21.21zM802.17 493.59v-22.62c0-8.28 6.72-15 15-15h82.76c8.28 0 15 6.72 15 15v22.62c0 8.28-6.72 15-15 15h-82.76c-8.28 0-15-6.72-15-15zM717.24 273.44l-16-16c-5.86-5.86-5.86-15.36 0-21.21l58.52-58.52c5.86-5.86 15.36-5.86 21.21 0l16 16c5.86 5.86 5.86 15.36 0 21.21l-58.52 58.52c-5.86 5.86-15.35 5.86-21.21 0z" fill="#F9C626" p-id="2741"></path></svg> : <svg className="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4199" width="25" height="25"><path d="M335.22 240.91c0-57.08 10.68-111.66 30.15-161.87-167.51 64.86-286.3 227.51-286.3 417.92 0 247.42 200.58 448 448 448 190.34 0 352.95-118.71 417.85-286.13-50.16 19.42-104.69 30.08-161.71 30.08-247.41 0-447.99-200.57-447.99-448z" fill="#d4237a" p-id="4200"></path></svg>}
                                </button>
                            </span>
                            <span className='search_bank' onClick={() => back_search()}>
                                <SearchOutlined />
                            </span>
                        </div>
                    </div> : 
                    <div className='head_right1' id={style.dark}>
                        <Button type="primary" onClick={showDrawer}>
                            {
                                flag ? <svg  className="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="3352" width="36" height="36"><path d="M892.928 128q28.672 0 48.64 19.968t19.968 48.64l0 52.224q0 28.672-19.968 48.64t-48.64 19.968l-759.808 0q-28.672 0-48.64-19.968t-19.968-48.64l0-52.224q0-28.672 19.968-48.64t48.64-19.968l759.808 0zM892.928 448.512q28.672 0 48.64 19.968t19.968 48.64l0 52.224q0 28.672-19.968 48.64t-48.64 19.968l-759.808 0q-28.672 0-48.64-19.968t-19.968-48.64l0-52.224q0-28.672 19.968-48.64t48.64-19.968l759.808 0zM892.928 769.024q28.672 0 48.64 19.968t19.968 48.64l0 52.224q0 28.672-19.968 48.64t-48.64 19.968l-759.808 0q-28.672 0-48.64-19.968t-19.968-48.64l0-52.224q0-28.672 19.968-48.64t48.64-19.968l759.808 0z" p-id="3353" fill="#f070aa"></path></svg>:
                                <svg className="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4526" width="36" height="36"><path d="M639.125 512l217.96875-217.96875c14.4375-14.4375 14.4375-40.03125 0-54.46875l-72.65625-72.65625c-14.4375-14.4375-40.125-14.4375-54.46875 0L512 384.78125 294.03125 166.8125c-14.4375-14.4375-40.125-14.4375-54.46875 0l-72.65625 72.65625c-14.4375 14.4375-14.4375 40.125 0 54.46875L384.78125 512 166.8125 729.96875c-14.4375 14.4375-14.4375 40.125 0 54.46875l72.65625 72.65625c14.4375 14.4375 40.125 14.4375 54.46875 0L512 639.125l217.96875 217.96875c14.4375 14.4375 40.125 14.4375 54.46875 0l72.65625-72.65625c14.4375-14.4375 14.4375-40.03125 0-54.46875L639.125 512z" p-id="4527" fill="#f070aa"></path></svg>
                            }
                        </Button>
                        <Drawer mask={false} placement="top" onClose={onClose} open={open} closable={false} >
                            <NavLink></NavLink>
                            <div className='none_box_language'>
                                <span className='language' onClick={() => change_language()}>
                                    <TranslationOutlined />
                                </span>
                            </div>
                            <div className='none_box_theme'>
                                <span className='theme'>
                                    <button className='theme_btn' onClick={() => {
                                        setTheme(theme === "白" ? "黑" : "白")
                                    }}>{theme === "白" ? <svg className="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="2739" width="38" height="38"><path d="M501.48 493.55m-233.03 0a233.03 233.03 0 1 0 466.06 0 233.03 233.03 0 1 0-466.06 0Z" fill="#F9C626" p-id="2740"></path><path d="M501.52 185.35H478.9c-8.28 0-15-6.72-15-15V87.59c0-8.28 6.72-15 15-15h22.62c8.28 0 15 6.72 15 15v82.76c0 8.28-6.72 15-15 15zM281.37 262.76l-16 16c-5.86 5.86-15.36 5.86-21.21 0l-58.52-58.52c-5.86-5.86-5.86-15.36 0-21.21l16-16c5.86-5.86 15.36-5.86 21.21 0l58.52 58.52c5.86 5.86 5.86 15.35 0 21.21zM185.76 478.48v22.62c0 8.28-6.72 15-15 15H88c-8.28 0-15-6.72-15-15v-22.62c0-8.28 6.72-15 15-15h82.76c8.28 0 15 6.72 15 15zM270.69 698.63l16 16c5.86 5.86 5.86 15.36 0 21.21l-58.52 58.52c-5.86 5.86-15.36 5.86-21.21 0l-16-16c-5.86-5.86-5.86-15.36 0-21.21l58.52-58.52c5.85-5.86 15.35-5.86 21.21 0zM486.41 794.24h22.62c8.28 0 15 6.72 15 15V892c0 8.28-6.72 15-15 15h-22.62c-8.28 0-15-6.72-15-15v-82.76c0-8.28 6.72-15 15-15zM706.56 709.31l16-16c5.86-5.86 15.36-5.86 21.21 0l58.52 58.52c5.86 5.86 5.86 15.36 0 21.21l-16 16c-5.86 5.86-15.36 5.86-21.21 0l-58.52-58.52c-5.86-5.85-5.86-15.35 0-21.21zM802.17 493.59v-22.62c0-8.28 6.72-15 15-15h82.76c8.28 0 15 6.72 15 15v22.62c0 8.28-6.72 15-15 15h-82.76c-8.28 0-15-6.72-15-15zM717.24 273.44l-16-16c-5.86-5.86-5.86-15.36 0-21.21l58.52-58.52c5.86-5.86 15.36-5.86 21.21 0l16 16c5.86 5.86 5.86 15.36 0 21.21l-58.52 58.52c-5.86 5.86-15.35 5.86-21.21 0z" fill="#F9C626" p-id="2741"></path></svg> : 
                                        <svg className="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4199" width="30" height="30"><path d="M335.22 240.91c0-57.08 10.68-111.66 30.15-161.87-167.51 64.86-286.3 227.51-286.3 417.92 0 247.42 200.58 448 448 448 190.34 0 352.95-118.71 417.85-286.13-50.16 19.42-104.69 30.08-161.71 30.08-247.41 0-447.99-200.57-447.99-448z" fill="#d4237a" p-id="4200"></path></svg>}
                                    </button>
                                </span>
                            </div>
                            <div className='none_box_search'>
                                <span className='search_bank' onClick={() => back_search()}>
                                    <SearchOutlined />
                                </span>
                            </div>
                        </Drawer>
                    </div>
            }
            

        </div>
    )
}

export default HeadTop