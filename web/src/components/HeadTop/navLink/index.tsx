import React from 'react'
import { NavLink, useLocation } from 'react-router-dom'
import style from "../../../css/index.module.css"
import { useTranslation } from "react-i18next"

type Props = {}

const index = (props: Props) => {
    const { pathname } = useLocation();
    const { t } = useTranslation()
    return (
        <div className='tabBar'>
            <ul id={style.dark}>
                <li id={style.router}>
                    <NavLink to="/home/index">
                        <span id={pathname == "/home/index" ? 'on' : style.router}>{t('artical')}</span>
                    </NavLink>
                </li>
                <li id={style.router}>
                    <NavLink to="/home/artives">
                        <span id={pathname == "/home/artives" ? 'on' : style.router}>{t('archives')}</span>
                    </NavLink>
                </li>
                <li id={style.router}>
                    <NavLink to="/home/know">
                        <span id={pathname == "/home/know" ? 'on' : style.router}>{t("know")}</span>
                    </NavLink>
                </li>
            </ul>
        </div>
    )
}

export default index