import React from 'react'
import { SmileOutlined } from '@ant-design/icons';
import { Input } from 'antd';
import  "./index_input.css"

type Props ={
    change_Discuss:any,
    chang_Flag:any,
    flag:boolean
}

const index = (props:Props) => {
    let {change_Discuss,flag,chang_Flag} = props
    const { TextArea } = Input;
    return (
        <div>
            <TextArea onClick={() => flag ? "" : chang_Flag()} onChange={(e)=>change_Discuss(e)} style={{ height: 120,width:760 }} rows={6} placeholder="请输入评论内容" maxLength={200}/>
            <div className='details_left_smile'>
                <SmileOutlined /> 表情
            </div>
        </div>
    )
}

export default index