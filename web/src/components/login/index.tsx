import { Modal, Input, Button } from 'antd';
import './login.css'
import { GithubOutlined } from '@ant-design/icons';
import { useState } from 'react';

type Props = {
    isLoginModalOpen: boolean,
    MyhandleOk: () => void,
    MyhandleCancel: () => void,
    MyMessage:(user:string,email:string) => void
}

const Login = (props: Props) => {
    const { isLoginModalOpen, MyhandleOk, MyhandleCancel, MyMessage } = props
    const [userValue,setUser] = useState("")
    const [emailValue,setEmail] = useState("")

    const letOk = ()=>{
        MyhandleCancel()
        MyMessage(userValue,emailValue)
    }
    const cancelOut = ()=>{
        MyhandleCancel()
    }
    return (
        <div className='login'>
            <Modal
                footer
                title="请设置你的信息"
                width={370}
                open={isLoginModalOpen}
                onOk={MyhandleOk}
                onCancel={MyhandleCancel}
                okText={'设置'}
                cancelText={'取消'}
            >
                <div className='login_content'>
                    <Input onBlur={(e)=>{setUser(e.target.value)}} placeholder="名称" className='Loginp' />
                    <Input onBlur={(e)=>{setEmail(e.target.value)}} placeholder="邮箱" className='Loginp' />
                    <Button type="primary" danger block onClick={() =>cancelOut()}>
                        取消
                    </Button>
                    <Button type="primary" danger block onClick={() =>letOk()}>
                        设置
                    </Button>
                    <div className='login_iconBox'>
                        <GithubOutlined/>
                    </div>
                </div>
            </Modal>
        </div>
    )
}

export default Login