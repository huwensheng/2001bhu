import { all, RducerItem } from "../../utils/types"
import { add_comment } from "../../api/axios"
import { message } from "antd"

const initialState:RducerItem = {
    articleList: [],
    flag:true,
    Tabindex: 0,
    details_item:[],
    List:[],
    imgList:[],
    headList:[],
    detailList:{},
    searchList:[],
    publish_data: [],
    xiqdetails_item:[],
    Share_id: '',
    commentlist:[]
}

const reducer = (state = initialState, { type, payload }: any) => {
    switch (type) {

        case "IS_LIKE" :
            state.articleList.forEach((item,index)=>{
                if(item.id===payload){
                    item.isRecommended = !item.isRecommended
                    
                    if(item.isRecommended){
                        item.likes++
                        console.log(1111);
                        
                    }else{
                        item.likes--
                    }
                    state.details_item[0] = item
                }
            })
            return {
                ...state,
                articleList:state.articleList,
                details_item:state.details_item
            }
        case "DEATILS_FIND" :
            state.details_item = state.articleList.filter((item,index)=>{
                return item.id === payload
            })
            console.log(state.details_item);
            return {
                ...state,
                details_item : state.details_item
            }
        
        case "GETARTICLELIST":
            state.articleList = payload
            return {
                ...state 
            }

            // 评论
            case "GETCOMMENTLIST":
                return{
                    ...state,
                    commentlist:[...payload],
                }


        case "TABITEM":
            state.articleList = state.articleList.filter((value: all, index: number) => {
                if (value.category) {
                return value.category.label == payload;
                }
            })
            return {
                ...state 
            }

        case "givegood":
            state.articleList.map((item: all, index: number) => {
                if (item.id == payload) {
                    if (item.isPay) {
                        item.likes--
                        item.isPay = !item.isPay;
                    } else {
                        item.likes++
                        item.isPay = !item.isPay;
                    }
                    return item
                }
            })
            return {
                ...state 
            }

        case "GET_LIST":
            return { 
                ...state,
                List:[...payload],
            }

        case "IMG_LIST":
            return { 
                ...state,
                imgList:[...payload],
            }
            
        case "HEAD_LIST":
            return { 
                ...state,
                headList:[...payload],
            }
            
        case 'GOTO_DETAIL':
            state.imgList.forEach((item: any) => {
                if (item.id === payload) {
                    state.detailList = item
                }
            })
            return {
                ...state,
                detailList: state.detailList
            }
        
        case "GET_DATA":
            return { 
                ...state,
                publish_data:[...payload],
            }

        case "SEARCH_FIND" :
            state.searchList = state.articleList.filter((item)=>{
                return item.title.includes( payload )
            })
            return {
                ...state,
                searchList : state.searchList
            }

        case "GET_DATA":
            return { 
                ...state,
                publish_data:[...payload],
            }

        case "XIQDEATILS_FIND" :
            return {
                ...state,
                xiqdetails_item : state.xiqdetails_item
            }

        case 'SHARE_ID':
            state.Share_id = payload;
            return { ...state }

        case "ADD_DISCUSS":
            console.log(11111,"后台");
            add_comment(payload).then((res:any)=>{
                if(res.statusCode===201){
                    message.success('评论成功')
                }else{
                    message.warning('评论正在审核')
                }
            })
            return {
                ...state,
                commentList:state.commentlist
            }
            
        default:
            return {
                ...state
            }
    }
}

export default reducer