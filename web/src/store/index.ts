import {
    legacy_createStore,
    combineReducers,
    applyMiddleware
} from "redux"

import thunk from "redux-thunk"

import logger from "redux-logger"

import reducer from "./module/reducer"
import redux from "./knowdecer/redux"

const All_reducer = combineReducers({
    reducer,
    redux
})

const store = legacy_createStore(All_reducer, applyMiddleware(thunk, logger))

export default store