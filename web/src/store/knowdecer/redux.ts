import {ALLACTION,DATATYPE,KNOWTYPE,KNOWSHARETYPE,KNOWLISTTYPE,KNOWSHAREIDTYPA} from "../../components/pop-up/know.d"
const initialState = {
    knowStoreList:[],
    isModalOpen:false,
    KnowShare_item:''
}

export default (state:DATATYPE = initialState, { type, payload }:ALLACTION) => {
  switch (type) {

  case KNOWTYPE:
      let data=payload as KNOWLISTTYPE[]
      let knowlist=data.filter((item:KNOWLISTTYPE)=>{
          return item.status as string=="publish"
      })

    return { 
        ...state,
        knowStoreList:knowlist
    }
  case KNOWSHARETYPE:
    let flag=payload
    return {
      ...state,
      isModalOpen:flag
    }

  case KNOWSHAREIDTYPA:
    return {
      ...state,
      KnowShare_item:payload
    }

  default:
    return state
  }
}