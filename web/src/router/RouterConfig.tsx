import { lazy } from "react"

export let mainRouter = [
    {
        path:'/home/index',
        name:'index',
        component:lazy(()=>import("../views/home/Index"))
    },
    {
        path:'/home/details/:id',
        name:'details',
        component:lazy(()=>import("../views/home/Details"))
    },
    {
        path:'/home/know',
        name:'know',
        component:lazy(()=>import("../views/home/Know"))
    },
    {
        path:'/home/artives',
        name:'artives',
        component:lazy(()=>import("../views/home/Artives"))
    },
    {
        path:'/home/knowdetails:id',
        name:'knowdetails',
        component:lazy(()=>import("../views/home/KnowDetails"))
    },
    {
        path:'/home/xi:id',
        name:'xi',
        component:lazy(()=>import("../views/home/Xiknow"))
    },
    {
        path:'/home/tagDetails/:id',
        name:'TagTitleDetails',
        component:lazy(()=>import("../views/home/TagTitleDetails/index"))
    },
]

const routes = [
    {
        path:'/home',
        name:'home',
        component:lazy(()=>import("../views/Home")),
        children:mainRouter
    },
    {
        path:'/search',
        name:'search',
        component:lazy(()=>import("../views/Search")),
    },
    {
        path:'/',
        redirect:'/home/index'
    }
]

export default routes