import React,{Suspense} from 'react'
import {
    BrowserRouter,
    Routes,
    Route,
    Navigate
} from "react-router-dom"
import routes from "./RouterConfig"
import {RootRoute} from "../utils/types"
import style from "../css/index.module.css"

type Props = {}

const RouterView = (props: Props) => {
    const renderRoute = (arr:RootRoute) =>{
        return arr.map((item,index)=>{
            return <Route
                    key={index}
                    path={item.path}
                    element={item.redirect?<Navigate to={item.redirect}/>:<item.component/>}
                    >
                        {
                            item.children&&renderRoute(item.children)
                        }
                    </Route>
        })
    }
    return (
        <div id='app' className={style.app}>
            <Suspense fallback={<div>页面加载中......</div>}>
                <BrowserRouter>
                    <Routes>
                        {
                            renderRoute(routes)
                        }
                    </Routes>
                </BrowserRouter>
            </Suspense>
        </div>
    )
}

export default RouterView