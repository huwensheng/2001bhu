import i18n from "i18next"

import {initReactI18next} from "react-i18next"

import LanguageDetector from "i18next-browser-languagedetector"

import ch from "./config/ch.config"

import en from "./config/en.config"

// 定义语言资源
const resources = {
    cn:{translation:ch},
    "zh-EN":{translation:en}
}

// 初始化
i18n.use(initReactI18next)
    .use(LanguageDetector)
    .init({
        resources,
        detection:{
            order: [
                'querystring',
                'cookie', 
                'localStorage', 
                'sessionStorage', 
                'navigator', 
                'htmlTag', 
                'path', 
                'subdomain'
            ]
        },
        interpolation:{
            escapeValue:false
        }
    })


export default i18n