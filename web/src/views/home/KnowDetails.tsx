
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate, useParams } from 'react-router-dom'
import * as api from "../../api/action"
import "../../css/Know/index.scss"
import {
  EyeOutlined,
  ForkOutlined
} from '@ant-design/icons';


const KnowDetails = () => {
  const params = useParams()
  const dispatch: any = useDispatch()
  const navigate = useNavigate()
  useEffect(() => {
    dispatch(api.goto_detail(params.id as string))
  }, [dispatch])

  useEffect(() => {
    dispatch(api.imglist_action())
  }, [dispatch])
  const { detailList, imgList } = useSelector(({ reducer }: any) => {
    return {
      ...reducer
    }
  })
  //  const xiq=(id:any)=>{
  //   navigate(`/home/knowdetailss/${id}`, { state: id })
  //   dispatch(xiqdetails_find(id))
  //  }
  return (
    <div className='detailscss'>
      <div className='detailscssleft'>
        <div className='know_loger_theme'>
          knowledgeBooks / {detailList.title}
        </div>
        <div className='know_loger_title'>
          {detailList.title}
        </div>
        <div className='know_loger_img'>
          <img src={detailList.cover} alt="" />
          <div className='know_loger_title_text'>
            {detailList.title}
          </div>
          <div className='know_loger_label'>
            {detailList.summary}
          </div>
          <div className='know_loger_time'>
            <span className='know_loger_time_num'>
              {detailList.order}
            </span>
            <span className='know_loger_time_text'>reading</span>
            <span className='know_loger_time_parmot'>·</span>
            <span className='know_loger_timeout'></span>
            {detailList.publishAt}
          </div>
          <div className='know_loger_btn_box'>
            <button disabled className='know_loger_btn'>
              Start Reading
            </button>
          </div>
          <div className='know_loger_footer'>
            Coming soon
          </div>
        </div>
      </div>

      <div className='detailscssright'>
        <div className='know_loger_right_taber'>otherKnowledges</div>
        <div className='know_loger_right_main'>
          {
            imgList.map((item: any, index: any) => {
              if (item.id != detailList.id) {
                return item.cover ?
                  <div className='know_loger_right_img' key={index} >
                    <div className='know_loger_right_item' onClick={() => {
                      navigate("/home/xi" + item.id)
                    }}>{item.title}
                    </div>
                    <dl className={index===imgList.length-1?"":"know_loger_right_imgdl"}>
                      <dt>{
                        item.cover ? <img src={item.cover} alt="" className='nav4' /> : ""
                      }
                      </dt>
                      <dd>
                        <div className='dd_title'>
                          {item.summary}
                        </div>
                        <div className='dd_farams'>
                          <span>0</span>
                          <EyeOutlined />·<ForkOutlined />
                          <span>分享</span>
                        </div>
                      </dd>
                    </dl>
                  </div> : ""
              }})
          }
        </div>
      </div>
    </div>
  )
}

export default KnowDetails