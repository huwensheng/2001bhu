import React, { useEffect } from 'react'
import "../../css/Artives/index.scss"
import { useDispatch, useSelector } from 'react-redux'
import * as api from "../../api/action"
import { useNavigate } from 'react-router-dom';
import { details_find, getArticlelist_action } from '../../api/action';
import style from "../../css/index.module.css"
import useCilen from "../../hooks/useClient"


type Props = {}

const Artives = (props: Props) => {
    const dispatch: any = useDispatch()
    const navigate = useNavigate()
    useEffect(() => {
        dispatch(api.getArticlelist_action_data())
    }, [dispatch])

    const size = useCilen()
    useEffect(() => {
        dispatch(api.imglist_action())
    }, [dispatch])

    // useEffect(()=>{
    //   dispatch(api.headlist_action())
    // },[dispatch])
    useEffect(() => {
        dispatch(getArticlelist_action())
    }, [])
    useEffect(() => {
        dispatch(api.get_data())
    }, [])

    const { List, imgList, articleList, publish_data } = useSelector(({ reducer }: any) => {
        return {
            ...reducer
        }
    })
    const bank_details = (id: string) => {
        navigate(`/home/details/${id}`, { state: id })
        dispatch(details_find(id))
    }
    return (
        <div className='artives'>
            <div className='left_artives' id={style.dark_min}>
                <div className="archives_top">
                    <p className='archives_top_tag'>archives</p>
                    <p className='archives_top_text'>total <span>{publish_data ? publish_data.length : ''}</span> piece </p>

                </div>
                <div className="archives_main">
                    <p className='archives_main_text1'>2022</p>
                    <p className='archives_main_text2'>September</p>
                    <ul className='archives_ul'>
                        {
                            publish_data ? publish_data.map((item: any, index: number) => {
                                return <li
                                    style={{ cursor: 'pointer' }}
                                    key={index}
                                    onClick={() => bank_details(item.id)}>
                                    <span className='li_span_time'>
                                        {
                                            item.publishAt ? item.publishAt.slice(5, 10) : (item.publishAt as any).slice(0, 10)
                                        }
                                    </span>
                                    <span className='li_span_text' style={{ fontSize: '17px' }}> {item.title}</span>
                                </li>
                            }) : ''
                        }
                    </ul>
                </div>
            </div>
            {
                size.width > 970 ? <div className='right_artives'>
                    <div className='righthead' id={style.dark_min}>
                        <p className='rightheadh2'>recommendToReading</p>
                        {
                            articleList.map((item: any, index: number) => {
                                return <ul key={index}>
                                    <li className='rightheadlist' onClick={() => bank_details(item.id)
                                    }>{item.title}<span style={{ color: "#999", paddingLeft: "10px", display: "inline-block" }}>{Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60) < 24 ? `${'大约'}${Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60)}${'小时前'}` : `${'大约'}${Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60 / 24)}${'天前'}`}</span></li>
                                </ul>
                            })
                        }
                    </div>
                    <div className='rightmain' id={style.dark_min}>
                        <p className='tag_right'>Category</p>
                        {
                            List.map((item: any, index: number) => {
                                return <ul key={index}>
                                    <li className='siz' onClick={() => {
                                        navigate('/home/index', { state: item.label })
                                    }}>
                                        <span className='siz_label'>
                                            {item.label}
                                        </span>
                                        <span className='span'>
                                            total  {item.articleCount}
                                            <span className='span_text'>
                                                Count
                                            </span>
                                        </span>
                                    </li>
                                </ul>
                            })
                        }
                    </div>
                </div> : ""
            }

        </div>
    )
}

export default Artives