
import React, { useEffect, useState } from 'react'
import { useLocation, useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux';
import { RootDispatch, QUANJUITEM, all, TagTitlelists } from "../../utils/types"
import { getArticlelist_action, givegood, TabItem, details_find,ShareId , Tabindexs, TabFlg} from "../../api/action"
import { get_Category, get_TagTitle } from '../../api/axios'
import Swiper from "../../components/Swiper/index"
import "./Index/index.scss"
import { EyeOutlined } from '@ant-design/icons';
import style from "../../css/index.module.css"
import useClient from "../../hooks/useClient"

type Props = {}

const Index = () => {
    // 调取仓库数据
    const { articleList,Tabindex, flag  } = useSelector(({ reducer }: QUANJUITEM) => {
        return {
            ...reducer
        }
    })
    const size = useClient()
    const {state} = useLocation()
    // const [flag, setFlag] = useState(true);
    const [Tablist, setTabList] = useState<all[]>([]);
    const [TagTitlelist, setTagTitlelist] = useState<TagTitlelists[]>([]);
    // const [Tabindex, setTabindex] = useState(0);
    const dispatch: RootDispatch = useDispatch();
    const navigate = useNavigate();
    useEffect(() => {
        dispatch(getArticlelist_action())
    }, [])
    // if (!articleList.length) {
    //     dispatch(getArticlelist_action())
    // }
    if (!Tablist.length) {
        get_Category().then(res => {
            setTabList(res.data.data);
        })
    }
    if (!TagTitlelist.length) {
        get_TagTitle().then(res => {
            setTagTitlelist(res.data.data);
        })
    }
    const likeBtn = (id: string) => {
        dispatch(givegood(id))

    }
   
    const TabBtn = (index: number, title: string) => {
        dispatch(Tabindexs(index))
        if (title == 'all') {
            dispatch(getArticlelist_action())
            dispatch(TabFlg(true))
        } else {
            dispatch(TabFlg(false))

            // console.log(index, title);
            dispatch(TabItem(title))
        }
    }
    // 详情
    const detailBtn = (id: string,isCommentable:any) => {
        
        navigate(`/home/details/${id}`, { state: id })
        dispatch(details_find(id))
        
    }
    //分享
     const shareBtn = (id: string) => {
        const Sharebox: any = document.querySelector('.Sharebox');
        Sharebox.style.display = "block";
        dispatch(ShareId(id));
    }

      // tag详情
      const tagDetails = (id: string) => {
        navigate(`/home/tagDetails/${id}`, { state: id })

    }
    return (
        <div className='index' id={style.main}>
            <div className="lefts" id={style.dark_min}>
                {/*在写一个判断条件  判断是否为轮播图 */}
                {
                    flag ? <Swiper /> : <div className='topbox'>
                        <p><span>{Tablist[Tabindex - 1].label}</span>  &nbsp;categoryArticle</p>
                        <p>totalSearch <span>{Tablist[Tabindex - 1].articleCount}</span> piece</p>
                    </div>
                }

                <div className='tab' id={style.dark_min}>
                    <ul>
                        <li className={Tabindex == 0 ? 'on' : ''} onClick={() => TabBtn(0, 'all')}>all</li>
                        {
                            Tablist.map((item: any, index) => {
                                return <li key={item.id} className={Tabindex == index + 1 ? 'on' : ''} onClick={() => TabBtn(index + 1, item.label)}>
                                    {item.label}
                                </li>
                            })
                        }
                    </ul>
                </div>

                <div className='Text' id={style.dark_min}>
                    {articleList.length ? articleList.map((item: all) => {
                        return <dl id={style.dark_min} key={item.id}>
                             <br />
                            {/* //详情页跳转 */}
                            <b className='title4' onClick={() => detailBtn(item.id,item.isCommentable)}>{item.title}
                                <span>
                                    {item.category ? "  |  " : ''}
                                    {item.category ? item.category.label : ''}
                                    <span style={{ color: "#999", paddingLeft: "10px", display: "inline-block" }}>{Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60) < 24 ? `${'大约'}${Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60)}${'小时前'}` : `${'大约'}${Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60 / 24)}${'天前'}`}</span>
                                </span>
                            </b>
                            <div className='ptext'>
                                {item.cover ? <p className='p1' onClick={() => detailBtn(item.id, item.isCommentable)}> <img src={item.cover} alt="" />
                                </p> : ''}
                                <p className={item.cover ? item.summary ? 'p2' : "p2s" : ''}>

                                    {item.summary ? item.summary : ''}
                                    {item.summary ? <br /> : ''}


                                    <span onClick={() => likeBtn(item.id)} id="zan" className={item.isPay ? 'on' : ''}>♥</span> {item.likes}       &nbsp;
                                    <span><EyeOutlined /> {item.views}</span>&nbsp;
                                    <span onClick={() => shareBtn(item.id)}>
                                        <svg viewBox="64 64 896 896" focusable="false" data-icon="share-alt" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path></svg>share
                                    </span>
                                </p>
                            </div>
                        </dl>
                    }) : 'empty'}
                </div>
            </div>
            {
                size.width > 760 ?
                <div className="rigths">
                    <div className="rigth_top" id={style.dark_min}>
                        <h2 id={style.dark_min}>recommendToReading</h2>
                        <ul>
                            {
                                articleList.length && articleList.map((item: all, index: number) => {
                                    if (index <= 5) {
                                        return <li className='inli' onClick={() => detailBtn(item.id,item.isCommentable)} key={index}>{item.title}   &emsp;· <span style={{ paddingLeft: "10px", display: "inline-block" }}>{Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60) < 24 ? `${'大约'}${Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60)}${'小时前'}` : `${'大约'}${Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60 / 24)}${'天前'}`}</span></li>
                                    }
                                })
                            }
                        </ul>
                    </div>

                    <div className="rigth_buttom" id={style.dark_min}>
                        <h2 id={style.dark_min}>tagTitle</h2>
                        <ul>
                            {
                                TagTitlelist.length ? TagTitlelist.map((item, index) => {
                                    return <li key={item.id} onClick={() => tagDetails(item.label)}>
                                        {item.label}&nbsp;[{item.articleCount}]
                                    </li>
                                }) : ""
                            }
                        </ul>
                    </div>
                </div>:""
            }
        </div>
    )
}

export default Index