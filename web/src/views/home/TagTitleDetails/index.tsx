import React, { useEffect, useState } from 'react'
import "./css/index.css"
import style from "../../../css/index.module.css"
import { useLocation, useNavigate, useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux';
import { RootDispatch, QUANJUITEM, all, TagTitlelists } from "../../../utils/types"
import { getArticlelist_action, givegood, TabItem, details_find, ShareId, Tabindexs, TabFlg } from "../../../api/action"
import { get_Category, get_TagTitle } from '../../../api/axios';
import { EyeOutlined } from '@ant-design/icons';

const Index = () => {
    // 调取仓库数据
    const { articleList } = useSelector(({ reducer }: QUANJUITEM) => {
        return {
            ...reducer
        }
    })
    const { id } = useParams();
    const [arr, setArr] = useState([])


    const navigate = useNavigate();//路由
    const dispatch: RootDispatch = useDispatch();//仓库方法
    const [titles, setTitles] = useState<string>();//标题
    const [tabindex, setTabindex] = useState<number>(0);//下标
    const [Tablist, setTabList] = useState<all[]>([]);//文章分类
    const [TagTitlelist, setTagTitlelist] = useState<TagTitlelists[]>([]);//文章标签
    if (!Tablist.length) {
        get_Category().then(res => {
            setTabList(res.data.data);
        })
    }
    if (!TagTitlelist.length) {
        get_TagTitle().then(res => {
            setTagTitlelist(res.data.data);
        })
    }
    useEffect(() => {
        setTitles(id)
        const str = articleList.filter((item: all) => {
            if (item.category !== null) {
                // console.log(item);
                return item.category.label == id
            }

        })
        setArr(str)
        const indexs = articleList.findIndex((item: all) => {
            if (item.category !== null) {
                return item.category.label == id
            }
        });

        setTabindex(indexs)
    }, [])

    // 详情
    const detailBtn = (ids: string, isCommentable: any) => {
        navigate(`/home/details/${ids}`, { state: ids })
        dispatch(details_find(ids))

    }
    // 点赞
    const likeBtn = (ids: string) => {
        dispatch(givegood(ids))
    }

    //分享
    const shareBtn = (ids: string) => {
        const Sharebox: any = document.querySelector('.Sharebox');
        Sharebox.style.display = "block";
        dispatch(ShareId(ids));
    }
    // tab
    const Tabbtn = (ids: string, index: number) => {
        setTabindex(index);
        const str = articleList.filter((item: all) => {
            if (item.category !== null) {
                console.log(ids);
                return item.category.label == ids;
            }
        })
        setArr(str)
        setTitles(ids)
    }
    const tabTitleBtn = (title: string, index: number) => {
        dispatch(Tabindexs(index));
        navigate(`/home/index`);
        dispatch(TabItem(title))
        // dispatch(TabFlg(false))


    }
    return (
        <div className='Tagindex' id={style.main}>
            <div className="Tag_lefts" id={style.dark_min}>

                <div className='Tag_topbox' id={style.dark_min}>
                    <p>与<span>{titles}</span>  &nbsp;标签有关的文章</p>
                    <p>共搜索到 <span>{arr.length}</span> 篇</p>
                </div>
                <div className='komg' id={style.main}></div>
                <div className='Tag_tab' id={style.dark_min}>
                    <p>文章标签</p>
                    <ul>
                        {
                            TagTitlelist.length ? TagTitlelist.map((item, index) => {
                                return <li
                                    key={item.id} className={index == tabindex ? 'on' : ''} onClick={() => Tabbtn(item.label, index)}>
                                    {item.label}&nbsp;[{item.articleCount}]
                                </li>
                            }) : ''
                        }
                    </ul>
                </div>
                <div className='komg' id={style.main}></div>
                <div className='Tag_Text' id={style.dark_min}>
                    {arr.length ? arr.map((item: all) => {
                        return <dl id={style.dark_min} key={item.id}>
                            <br />
                            {/* //详情页跳转 */}
                            <b className='title4' onClick={() => detailBtn(item.id, item.isCommentable)}>{item.title}
                                <span>
                                    {item.category ? "  |  " : ''}
                                    {item.category ? item.category.label : ''}
                                    <span style={{ color: "#999", paddingLeft: "10px", display: "inline-block" }}>{Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60) < 24 ? `${'大约'}${Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60)}${'小时前'}` : `${'大约'}${Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60 / 24)}${'天前'}`}</span>
                                </span>
                            </b>

                            <div className='ptext'>
                                {item.cover ? <p className='p1' onClick={() => detailBtn(item.id, item.isCommentable)}> <img src={item.cover} alt="" />
                                </p> : ''}
                                <p className={item.cover ? item.summary ? 'p2' : "p2s" : ''}>

                                    {item.summary ? item.summary : ''}
                                    {item.summary ? <br /> : ''}


                                    <span onClick={() => likeBtn(item.id)} id="zan" className={item.isPay ? 'on' : ''}>♥</span> {item.likes}       &nbsp;
                                    <span><EyeOutlined /> {item.views}</span>&nbsp;
                                    <span onClick={() => shareBtn(item.id)}>
                                        <svg viewBox="64 64 896 896" focusable="false" data-icon="share-alt" width="1em" height="1em" fill="currentColor" aria-hidden="true"><path d="M752 664c-28.5 0-54.8 10-75.4 26.7L469.4 540.8a160.68 160.68 0 000-57.6l207.2-149.9C697.2 350 723.5 360 752 360c66.2 0 120-53.8 120-120s-53.8-120-120-120-120 53.8-120 120c0 11.6 1.6 22.7 4.7 33.3L439.9 415.8C410.7 377.1 364.3 352 312 352c-88.4 0-160 71.6-160 160s71.6 160 160 160c52.3 0 98.7-25.1 127.9-63.8l196.8 142.5c-3.1 10.6-4.7 21.8-4.7 33.3 0 66.2 53.8 120 120 120s120-53.8 120-120-53.8-120-120-120zm0-476c28.7 0 52 23.3 52 52s-23.3 52-52 52-52-23.3-52-52 23.3-52 52-52zM312 600c-48.5 0-88-39.5-88-88s39.5-88 88-88 88 39.5 88 88-39.5 88-88 88zm440 236c-28.7 0-52-23.3-52-52s23.3-52 52-52 52 23.3 52 52-23.3 52-52 52z"></path></svg>share
                                    </span>
                                </p>
                            </div>

                        </dl>
                    }) : 'empty'}
                </div>
            </div>
            <div className="Tag_rigths">
                <div className="Tag_rigth_top" id={style.dark_min}>
                    <h2 id={style.dark_min}>推荐阅读</h2>
                    <ul>
                        {
                            articleList.length ? articleList.map((item: all, index: number) => {
                                if (index <= 5) {
                                    return <li className='inli' onClick={() => detailBtn(item.id, item.isCommentable)} key={index}>{item.title}   &emsp;· <span style={{ paddingLeft: "10px", display: "inline-block" }}>{Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60) < 24 ? `${'大约'}${Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60)}${'小时前'}` : `${'大约'}${Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60 / 24)}${'天前'}`}</span></li>
                                }
                            }) : "暂无推荐"
                        }
                    </ul>
                </div>

                <div className="Tag_rigth_buttom" id={style.dark_min}>
                    <h2 id={style.dark_min}>文章分类</h2>
                    <ul>
                        {
                            Tablist.length ? Tablist.map((item: all, index: number) => {
                                return <li key={item.id} onClick={() => tabTitleBtn(item.label, index + 1)}>
                                    <span>{item.label}</span>
                                    <span>共计{item.articleCount}篇文章</span>
                                </li>
                            }) : "暂无分类"
                        }
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default Index;
