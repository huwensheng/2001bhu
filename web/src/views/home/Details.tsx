import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { RootDispatch } from "../../utils/types"
import { RootState, QUANJUITEM, all } from "../../utils/types"
import "./details/css/style.scss"
import "../../../src/css/markdown.css"
// 吸顶
import useClient from '../../hooks/useClient'
import style from "../../css/index.module.css"
import { HeartFilled, CommentOutlined, ForkOutlined } from '@ant-design/icons';
import { useParams } from "react-router-dom"
import { details_find, isLike, getCommentlist, add_discuss, ShareId } from "../../api/action"
import { Anchor, Image, Input, message, Button, Pagination } from 'antd';
//时间
import { date } from "../../utils/time"
//支付
// 评论
import Comments from '../../components/comments'
//吸顶
import useScroll from "../../hooks/onScroll"
import Login from "../../components/login/index"
// 此组件不可删除
import Paylout from "../../components/PayMoney/index"
import CommentInputBox from "../../components/commentInputBox"
import * as api from "../../api/action";
//分页


type Props = {}
const { TextArea } = Input;

const Details = (props: Props) => {
    const [flag, setFlag] = useState(false)

    // 评论数据
    useEffect(() => {
        dispatch(getCommentlist())
    }, [])
    const { commentlist } = useSelector(({ reducer }: QUANJUITEM) => {
        return {
            ...reducer
        }
    })

    // 详情页数据
    const scroll = useScroll()
    // 详情页数据
    const dispatch: RootDispatch = useDispatch()
    const { id } = useParams()
    // useSelector  获取仓库中对应的id的数据
    const details_item = useSelector((state: RootState) => {
        return state.reducer.details_item
    })
    useEffect(() => {
        dispatch(details_find(id as string))
    }, [dispatch])
    const chang_like = () => {
        dispatch(isLike(id as string))
    }

    const { articleList } = useSelector(({ reducer }: QUANJUITEM) => {
        return {
            ...reducer
        }
    })

    //支付弹窗

    // 弹窗


    const showModal = () => {
        const Sharebox: any = document.querySelector('.Sharebox');
        console.log(Sharebox);

        Sharebox.style.display = "block";
        dispatch(ShareId(id as string));
    };

    const [isLoginModalOpen, setIsLoginModalOpen] = useState(false);
    const isLoginout = () => {
        setIsLoginModalOpen(true)
    }
    const { Link } = Anchor;

    const handleCancelAnchor = (e: any) => {
        e.preventDefault()
    }

    // 评论
    const [pageSize, setPageSize] = useState(1);
    const [limit] = useState(5);
    const pagiList = commentlist.slice((pageSize - 1) * limit, pageSize * limit);
    useEffect(() => {
        dispatch(api.getCommentlist());
    }, []);
    const [textval, setTextval] = useState('');

 
    
    //  吸顶
    const size = useClient()
    //    表情
    // const emojilist = [
    //     { id: 1, emoji: '😀' },
    //     { id: 2, emoji: '😃' },
    //     { id: 3, emoji: '😄' },
    //     { id: 4, emoji: '😁' },
    //     { id: 6, emoji: '😆' },
    //     { id: 7, emoji: '😅' },
    //     { id: 8, emoji: '😂' },
    //     { id: 9, emoji: '😍' },
    //     { id: 10, emoji: '😘' },
    //     { id: 11, emoji: '😉' },
    //     { id: 12, emoji: '😊' },
    //     { id: 13, emoji: '😇' },
    // ]
    const bank_details = (id: string) => {
        dispatch(details_find(id))
    }
    const [value, setValue] = useState("")
    const [username, setUsername] = useState("")
    const [email, setEmail] = useState("")
    const addDiscuss = () => {
        // console.log(111,"详情页");
        let url = `/article/${id}`
        dispatch(add_discuss(value, username, email,id as string,url))
    }

    const change_Discuss = (e: any) => {
        setValue(e.target.value)
        setTextval(value)
    }
    const handleOk = ()=>{
        setIsLoginModalOpen(true)
        setFlag(true)
    }
    const handleCancel = ()=>{
        setIsLoginModalOpen(false)
    }
    const change_massage = (user: string, email: string)=>{
        setUsername(user)
        setEmail(email)
        setFlag(true)
    }
    //分页
    
    return (
        <div className='details'>
            {/* 不可删除 */}
            {/* {
                details_item[0].views <= 0 ? "" : <Paylout details_item={details_item}></Paylout>
            } */}
            <div className='details_main'>
                <div className='details_left' id={style.dark_min}>
                    {/* 渲染图片 */}
                    <Image src={details_item[0].cover} />

                    {/* 渲染标题 */}
                    <h2 className='details_left_title'>{details_item[0].title}</h2>

                    {/* 时间 */}
                    <p className='details_left_updateAt'><i><span>发布于 {date(details_item[0].publishAt)}</span> • <span>readings{details_item[0].views}</span></i></p>

                    {/* dangerouslySetInnerHTML={{__html:(要渲染的数据)}} */}
                    <div className='markdown' dangerouslySetInnerHTML={{ __html: details_item[0].html }}></div>
                    {/* 评论 */}
                    <div id='details_left_review'>
                        <div className='details_left_review_rea'>评论</div>
                        {/* {comments.length > 0 && <CommentList comments={comments} />} */}
                        {/* 判断输入框中是否有数据，如果为空发布按钮为禁用 */}
                        <p className='details_left_onSubmit'>
                            {/* {details_item[0].isRecommended ?""  : ""}*/}
                            {/* 表情 */}
                            <p className='details_left_onSubmit_input'>
                                {
                                    details_item[0].isCommentable ?
                                        <div>
                                            <CommentInputBox chang_Flag={() => {
                                                setIsLoginModalOpen(true)
                                            }} flag={flag} change_Discuss={change_Discuss}></CommentInputBox>
                                        </div> : ""
                                }
                                {
                                    textval.length < 0 ?
                                        <Button className='details_left_btn' type="primary" disabled={false}>
                                            发布
                                        </Button> :
                                        <Button className='details_left_btn' type="primary" onClick={()=>{
                                            flag ? addDiscuss() : isLoginout()
                                        }}>
                                            发布
                                        </Button>
                                }
                            </p>
                        </p>
                        {/* 评论数据渲染 */}
                        {
                            details_item[0].isCommentable ?
                                <div>
                                    <Comments list={pagiList} id={details_item[0].id}/>
                                </div>
                                : ""
                        }
                        {/* 评论分页 */}
                        <div className='pagination'>
                            {commentlist.length === 0 ? (
                                ""
                            ) : (
                                <Pagination
                                    className="pagination"
                                    onChange={(page) => setPageSize(page)}
                                    defaultCurrent={1}
                                    total={commentlist.length}
                                    pageSize={limit}
                                    current={pageSize}
                                />
                            )}
                        </div>
                    </div>
                    {/* recommendToReading  */}
                </div>
                {
                    size.width > 760 ? <div className='details_right'>
                        <div className='details_right_fixation' id={scroll.height >= 100 ? 'details_right_fix' : ''}>
                            <div className='details_right_comment' id={style.dark_min}>

                                <h2 className='details_right_recomment'>Recommend Readings</h2>
                                <ul className='details_right_comment_ulList'>
                                    {
                                        articleList.length && articleList.map((item: all, index: number) => {

                                            if (item.id != details_item[0].id && item.isCommentable == true) {

                                                return <li onClick={() => bank_details(item.id)} key={index}>{item.title}
                                                    <span className='partition'>·</span>
                                                    <span className='time_out'>
                                                        {Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60) < 24 ? `${'大约'}${Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60)}${'小时前'}` : `${'大约'}${Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60 / 24)}${'天前'}`}
                                                    </span>
                                                </li>
                                            }
                                        })
                                    }
                                </ul>
                            </div>
                            {/* toc */}
                            <div className='details_right_toc' id={style.dark_min}>
                                <h2 >toc</h2>
                                <Anchor affix={false} onClick={handleCancelAnchor} showInkInFixed={true}>

                                    {
                                        JSON.parse(details_item[0].toc).map((item: any, index: any) => {
                                            return <Link key={index} href={'#' + item.id} title={item.text} />
                                        })
                                    }
                                </Anchor>
                            </div>
                        </div>
                    </div> : ""
                }


            </div>
            <ul className={size.width > 760 ? 'details_postion' : "details_postion_footer"}>
                {/* 收藏 */}
                <li id={style.dark_min}>
                    <span className={details_item[0].isRecommended ? 'details_like_fla' : 'details_like'} onClick={() => chang_like()}>
                        <HeartFilled />
                        <span className='like_num'>{details_item[0].likes}</span>
                    </span>
                </li>
                {/* 评论 */}
                <li>
                    <Anchor bounds={0} onClick={handleCancelAnchor} className='details_news' showInkInFixed={false} affix={false}>
                        {/*  href="#details_left_review" 跳转到id名为#后边的东西 */}
                        <Link href="#details_left_review" title={<CommentOutlined className='wechat' />} />
                    </Anchor>
                </li>
                {/* 分享 */}
                <li id={style.dark_min}>
                    <span className='details_share'>
                        <ForkOutlined onClick={showModal} />
                    </span>
                </li>
            </ul>
            <Login
                isLoginModalOpen={isLoginModalOpen}
                MyhandleOk={() => handleOk()}
                MyhandleCancel={() => handleCancel()}
                MyMessage={change_massage}
            ></Login>

        </div>
    )
}

export default Details