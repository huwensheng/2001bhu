import React, { useEffect } from 'react'
import "../../css/Know/index.scss"
import { useDispatch, useSelector } from 'react-redux'
import * as api from "../../api/action"
import { useNavigate } from 'react-router-dom';
import { details_find, getArticlelist_action } from '../../api/action';
import Know_share from '../../components/pop-up/index'
import {
  EyeOutlined,
  ForkOutlined
} from '@ant-design/icons';
import style from "../../css/index.module.css"
import { KNOWLISTTYPE, KNOWSHAREIDTYPA, KNOWSHARETYPE } from '../../components/pop-up/know.d';
import useClient from '../../hooks/useClient';


const Know = () => {
  const dispatch: any = useDispatch()
  const navigate = useNavigate()
  const size = useClient()
  useEffect(() => {
    dispatch(api.getArticlelist_action_data())
  }, [dispatch])

  useEffect(() => {
    dispatch(api.imglist_action())
  }, [dispatch])

  useEffect(() => {
    dispatch(getArticlelist_action())
  }, [])

  useEffect(() => {
    dispatch(api.get_data())
  }, [])


  const { List, imgList, articleList } = useSelector(({ reducer }: any) => {
    return {
      ...reducer
    }
  })
  const xQing = (id: string) => {
    navigate(`/home/details/${id}`, { state: id })
    dispatch(details_find(id))
  }

  const showModal = (item: KNOWLISTTYPE, e: any) => {
    e.stopPropagation()
    console.log(item.id, "分享item");
    const Sharebox: any = document.querySelector('.Sharebox');
    Sharebox.style.display = "block";
    dispatch(api.ShareId(item.id));
  };


  return (
    <div className='know'>
      <div className='leftkonw' id={style.dark_min}>
        {
          imgList.map((item: any, index: number) => {
            return item.cover ? <ul key={index}
              onClick={() => {
                navigate("/home/knowdetails" + item.id)
              }}
              className={index === imgList.length - 1 ? "" : "leftkonw_ul_main"}>
              <div className='title'>
                {item.title}
              </div>
              <li>
                {
                  item.cover ?
                    <div className='left_konw_text_img'>
                      <div className='left_konw_text_cover'>
                        <img src={item.cover} alt="" className='cover' />
                      </div>
                      <div className='left_konw_text_mary'>
                        <p className='title1'>{item.summary}</p>
                        <p className='title2'><EyeOutlined /> {item.views} · <ForkOutlined /> <span onClick={(e) => showModal(item, e)}>分享</span></p>
                      </div>
                    </div> :
                    <div className='left_konw_text'>
                      <span className='title_text'>{item.title}</span>
                      <span className='title_eyeout'><EyeOutlined />  0 . <ForkOutlined /> <span onClick={(e) => showModal(item, e)}>分享</span></span>
                    </div>
                }
              </li>
            </ul> : ""
          })
        }
      </div>
      {
        size.width > 960 ? <div className='rightkonw'>
          <div className='righthead' id={style.dark_min}>
            <div className='rightheadh2'>recommendTo Reading</div>
            {
              articleList.map((item: any, index: number) => {
                return <ul key={index}>
                  <li className='rightheadlist' onClick={() => xQing(item.id)
                  }>{item.title}
                    <span style={{ color: "#999", paddingLeft: "10px", display: "inline-block" }}>{Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60) < 24 ? `${'大约'}${Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60)}${'小时前'}` : `${'大约'}${Math.round(((Date.now() - (new Date(item.createAt)).getTime())) / 1000 / 60 / 60 / 24)}${'天前'}`}</span>
                  </li>
                </ul>
              })
            }
          </div>
          <div className='rightmain' id={style.dark_min}>
            <div className='right_main_head'>Category</div>
            {
              List.map((item: any, index: number) => {
                return <ul key={index}>
                  <li className='siz' onClick={() => {
                    navigate('/home/index', { state: item.label })
                  }}>
                    <span className='siz_label'>
                      {item.label}
                    </span>
                    <span className='span'>
                      Total
                      <span>
                        {item.articleCount}
                      </span>
                      Count
                    </span>
                  </li>
                </ul>
              })
            }
          </div>
        </div> : ""
      }

      <Know_share></Know_share>
    </div>
  )
}

export default Know