import React from 'react'
import {
    Outlet
} from "react-router-dom"
import HeadTop from "../components/HeadTop/index"
import BackTop from "../components/banktop/Index"
import style from "../css/index.module.css"
import Sharebox from '../components/Sharebox'
type Props = {}

const Home = (props: Props) => {
    return (
        <div className='home'>
            <div className='home_nav' id={style.dark}>
                <nav className='home_navs' id={style.dark}>
                    <HeadTop></HeadTop>
                </nav>
            </div>
            <div className='home_main' id={style.main}>
                <div className='home_con' id={style.main}>
                    <Outlet></Outlet>
                </div>
            </div>
            <div className='home_foot' id={style.footer}>
                <div className="home_footer"></div>
            </div>
            {/* 返回顶部 */}
            <BackTop></BackTop>
            {/* 分享弹框 */}
            <Sharebox></Sharebox>
        </div>
    )
}

export default Home