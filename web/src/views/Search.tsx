import React from 'react'
import {CloseOutlined} from '@ant-design/icons';
import { useNavigate } from 'react-router-dom';
import { Input } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { RootDispatch, RootState } from '../utils/types';
import { search_find, details_find } from "../api/action"

const { Search } = Input;

type Props = {}


const Searchout = (props: Props) => {
    const dispatch:RootDispatch = useDispatch()
    const navigate = useNavigate()
    const searchList = useSelector((state:RootState)=>{
        return  state.reducer.searchList
    })
    const onSearch = (value: string) =>{
        dispatch(search_find(value))
    }
    const details_bank = (id:string)=>{
        navigate(`/home/details/${id}`, { state: id })
        dispatch(details_find(id))
    }
    return (
        <div className='search'>
            <div className='search_top'>
                <span className='search_top_left'>searchArticle</span>
                <span className='search_top_right'>
                    <button onClick={()=>{
                        navigate(-1)
                    }}>
                        <CloseOutlined />
                    </button>
                    <p>esc</p>
                </span>
            </div>

            <div id='search_text'>
                <Search placeholder="searchArticle" onSearch={onSearch}/>
            </div>

            <div className='search_result'>
                {
                    searchList.length?searchList.map((item,index)=>{
                        return <li 
                            key={index}
                            onClick={()=>details_bank(item.id)}
                        >
                            {item.title}
                        </li>
                    }) : ""
                }
            </div>

        </div>
    )
}

export default Searchout