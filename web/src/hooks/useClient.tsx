import React,{useCallback,useEffect, useState} from 'react'

const useClient = () => {
    const [size,setSize] = useState({
        width:window.document.documentElement.clientWidth,
        height:window.document.documentElement.clientHeight
    })
    const change_client = useCallback(()=>{
        setSize({
            width:window.document.documentElement.clientWidth,
            height:window.document.documentElement.clientHeight
        })
    },[])
    useEffect(()=>{
        window.addEventListener('resize',change_client)
        return (
             window.addEventListener('resize',change_client)
        )
    },[])
    return size
}

export default useClient