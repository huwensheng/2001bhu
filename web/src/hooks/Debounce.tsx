import React, { useRef } from "react"

interface ICurrent {
    fn: Function,
    timer: null | NodeJS.Timeout
}

// 防抖函数
export const useDebounce = (fn: Function, delay = 500, immediate?: boolean) => {
    const { current } = useRef<ICurrent>({ fn, timer: null }) //使用useRef来保存变量
    return function (...args: any[]) {
        // @ts-ignore
        const that = this;
        if (current.timer) {
            clearTimeout(current.timer)    //直接清除定时器（关键）
            current.timer = null
        }
        if (immediate) {
            let rightNow = !current
            current.timer = setTimeout(() => {
                current.timer = null
            }, delay)
            if (rightNow) {
                current.fn.apply(that, args)
            }
        } else {
            current.timer = setTimeout(() => {
                current.fn.apply(that, args)
            }, delay)
        }
    }
}
