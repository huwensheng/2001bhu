import { useState, useCallback, useEffect } from 'react'

const ceiling = () => {
    const [scroll, setSize] = useState({
        height: document.documentElement.scrollTop,
    });
    const changeSize = useCallback(() => {
        setSize({
            height: document.documentElement.scrollTop,
        })
    }, [])

    useEffect(() => {
        window.addEventListener("scroll", changeSize);
        return () => {
            window.removeEventListener("scroll", changeSize)
        }
    }, [])
    return scroll
}

export default ceiling