import React, { useRef } from "react"

interface ICurrent {
    fn: Function,
    timer: null | NodeJS.Timeout
}

// 节流函数
export const useThrottle = (fn: Function, delay = 500) => {
    const { current } = useRef<ICurrent>({ fn, timer: null })
    return function (...args: any[]) {
        if (!current.timer) {   //n秒内一直触发，timer一直为null，就不会执行判断语句中的逻辑（关键）
            current.timer = setTimeout(() => {
                // @ts-ignore
                current.fn.apply(this, args)
                    .current.timer = null
            }, delay)
        }
    }
}
