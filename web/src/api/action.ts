import axios from "../utils/request"
import { Dispatch } from "redux";
import { get_Articlelist, get_Articlelist_data, get_imglist, get_Commentlist } from "../api/axios"
export const getArticlelist_action = () => {
    return async (dispatch: Dispatch) => {
        const { data } = await get_Articlelist()
        if (data.statusCode == 200) {
            dispatch({
                type: "GETARTICLELIST",
                payload: data.data[0]
            })
        }
    }
}

// 评论数据
export const getCommentlist = () => {
    return async (dispatch: Dispatch) => {
        const { data } = await get_Commentlist()
        if (data.statusCode == 200) {
            dispatch({
                type: "GETCOMMENTLIST",
                payload: data.data[0]
            })
        }
    }
}

// 点赞
export const givegood = (id: string) => {
    return async (dispatch: Dispatch) => {
        dispatch({
            type: "givegood",
            payload: id
        })
    }
}

// tab切换
export const TabItem = (id: string) => {
    return async (dispatch: Dispatch) => {
        dispatch({
            type: "TABITEM",
            payload: id
        })
    }
}
// tab切换index
export const Tabindexs = (id: string | number) => {
    return async (dispatch: Dispatch) => {
        dispatch({
            type: "TABINDEXS",
            payload: id
        })
    }
}
// tab切换轮播还是？？
export const TabFlg= (id: boolean) => {
    return async (dispatch: Dispatch) => {
        dispatch({
            type: "TABFLAG",
            payload: id
        })
    }
}
// 分享
export const ShareId = (id: string) => {
    return async (dispatch: Dispatch) => {
        dispatch({
            type: 'SHARE_ID',
            payload: id,
        });
    };
};

export let details_find = (id: string) => {
    return {
        type: "DEATILS_FIND",
        payload: id
    }
}

export let isLike = (id: string) => {
    return {
        type: 'IS_LIKE',
        payload: id
    }
}

//konw右边数据
// export  let get_Artice=()=>axios.get("/api/article")
export const getArticlelist_action_data = () => {
    return async (dispatch: Dispatch) => {
        const { data } = await get_Articlelist_data()
        if (data.statusCode == 200) {
            dispatch({
                type: "GET_LIST",
                payload: data.data
            })
        }
    }
}

export const imglist_action = () => {
    return async (dispatch: Dispatch) => {
        const { data } = await get_imglist()
        if (data.statusCode == 200) {
            dispatch({
                type: "IMG_LIST",
                payload: data.data[0]
            })
        }
    }
}



export const goto_detail = (id: string) => {
    return ({
        type: 'GOTO_DETAIL',
        payload: id
    })
}


export const get_data = () => {
    return async (dispatch: any) => {
        await axios.get('/api/article').then(res => {
            dispatch({
                type: 'GET_DATA',
                payload: res.data.data[0]
            })
        })

    }
}

export let search_find = (value: string) => {
    return {
        type: 'SEARCH_FIND',
        payload: value
    }
}


export let xiqdetails_find = (id: string) => {
    return {
        type: "XIQDEATILS_FIND",
        payload: id
    }
}




export let add_discuss = (value:string,name:string,email:string,id:string,url:string) => {
    return {
        type:"ADD_DISCUSS",
        payload:{
            name:name,
            email:email,
            content:value,
            hostId:id,
            url:url
        }
    }
}