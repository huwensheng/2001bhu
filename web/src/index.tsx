import React from 'react';
import ReactDOM from 'react-dom/client';
import "./css/style.scss"
import RouterView from './router/RouterView';
import {Provider} from "react-redux"
import store from "./store/index"
import "./css/theme.css"
import "./index.css"

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterView />
    </Provider>
  </React.StrictMode>
);


