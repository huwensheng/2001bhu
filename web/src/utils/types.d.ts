import { JSXElement } from "react"


export type TagTitlelists = {
    articleCount: number,
    createAt: string,
    id: string,
    label: string,
    value: string,
    updateAt: string
}

export type all = any

export type QUANJUITEM = string | number | Array | Object | boolean;



export type RootRoute = Array<RootRouteItem>

export type RootRouteItem = {
    path: string,
    name?: string,
    component?: JSXElement,
    children?: RootRoute,
    redirect?: string
}

export type RootDispatch = typeof store.dispatch

export type RootState = {
    reducer : RducerItem
}

export type RducerItem = {
    articleList : Array< ArticleListItem >,
    flag: boolean,
    Tabindex: number,
    details_item : Array< ArticleListItem >,
    List : Array<StateList>,
    imgList : Array<StateList>,
    headList : any,
    detailList : any,
    searchList: Array< ArticleListItem >,
    publish_data: any,
    xiqdetails_item:any,
    Share_id:any,
    commentlist:any
}



export type StateList = {
    id : string,
    parentId : null,
    order : number,
    title : string,
    cover : string,
    summary : string,
    content : null,
    html : null,
    toc : null,
    status : string,
    views : number,
    likes : number,
    isCommentable : boolean,
    publishAt : string,
    createAt : string,
    updateAt : string
}




export type ArticleListItem = {
    id : string,
    category : CategoryItem,
    content : string,
    cover : string,
    createAt : string,
    publishAt  : string,
    html : string,
    isCommentable : boolean,//是否点赞
    isPay : boolean,//是否支付
    isRecommended : boolean,//是否推荐
    likes : number,//点赞数
    needPassword : boolean,
    status : string,
    summary : string,
    tags : Array<string>,
    title : string,
    toc : string,
    Amount : null,
    updateAt : string,
    views : number
}

export type CategoryItem = {
    id:string,
    createAt:string,
    updateAt:string,
    label:string,
    value:string,
}